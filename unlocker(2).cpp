#include <iostream>
#include <array>
#include <string>
#include <vector>
#include <cstring>

int main(){

    std::array< std::string, 100> starts = {"00","01","02","03","04","05","06","07","08","09"};

    for ( int i = 10; i < 100; i++ ){
        starts[i] = std::to_string(i);
    }

    //6 elements from starts
    std::vector<int> pos = {00,10,01,00,10,01};
    std::vector<int> comp = {99,99,99,99,99,99};
    std::string Final;
    int counter = 0;
    int bool0 = 1;
    int bool1 = 1;

    while ( ( pos != comp ) and bool1 ){

        bool0 = 1;
                
        for (int i = 5; i >=0 ; i--){
            if (pos[i] == 100){
                pos[i] = 0;
                pos[i-1] += 1;

            }
        }

        for (int i = 1; i < 6; i++){
            if ( ( starts[pos[i-1]][0] == starts[pos[i-1]][1] ) 
              and ( starts[pos[i-1]][0] == starts[pos[i]][0] ) ){
                bool0 = 0;
                break;
            
            }
            if ( ( starts[pos[i-1]][0] != starts[pos[i-1]][1] ) 
              and ( starts[pos[i-1]][1] == starts[pos[i]][0] ) 
              and ( starts[pos[i]][0] == starts[pos[i]][1] ) ){
                bool0 = 0;
                break;
            
            }
        }        
        
        if (bool0){

            Final = "fastboot oem unlock ";
            for (int i = 0; i < 6 ; i++){
                Final+= starts[pos[i]];
            }
            Final += " 2> /dev/null";

            if (counter == 1000){
                std::cout << Final << std::endl;
                counter = 0;
            }
            counter++;

            bool1 = system( Final.c_str() );
            if (!bool1){
                std::cout << "The right perm is " << Final << std::endl;
            }
        }
        pos[5] += 1;
    }
    return 0;

}